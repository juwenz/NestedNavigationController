//
//  AppDelegate.h
//  TestNestNavi
//
//  Created by juwenz on 13-11-19.
//  Copyright (c) 2013年 scics. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
