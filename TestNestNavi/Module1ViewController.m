//
//  Module1ViewController.m
//  TestNestNavi
//
//  Created by juwenz on 13-11-27.
//  Copyright (c) 2013年 scics. All rights reserved.
//

#import "Module1ViewController.h"

#import "Tab2ContentViewController.h"

@interface Module1ViewController (){
    UIWindow *window;
}

@end

@implementation Module1ViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewWillAppear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)back:(id)sender{
//    NSLog(@"%@",self.navigationController.viewControllers);
//    [window removeFromSuperview];
    //内存释放代码，注释后即可看到效果
    for (UIView *v in self.view.subviews) {
        if ([v isKindOfClass:[UIWindow class]]) {
            UIWindow *win = (UIWindow *)v;
            win.hidden = true;
            
            UITabBarController *tab = (UITabBarController*)win.rootViewController;
            tab.viewControllers = @[];
            tab = nil;
            
            [win removeFromSuperview];
            win.rootViewController = nil;
            
            win = nil;
        }
    }

    [self.navigationController popToRootViewControllerAnimated:YES];
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    CGRect bounds = self.view.bounds;
    Tab1ContentViewController *tab1ContentVC = [[Tab1ContentViewController alloc]init];
    tab1ContentVC.backDelegate = self;
    Tab2ContentViewController *tab2ContentVC = [[Tab2ContentViewController alloc]init];
    UINavigationController *navi1 = [[UINavigationController alloc]initWithRootViewController:tab1ContentVC];
    UINavigationController *navi2 = [[UINavigationController alloc]initWithRootViewController:tab2ContentVC];
    UITabBarController *tab = [[UITabBarController alloc]initWithNibName:@"UITabBarController" bundle:nil];
    
    tab.viewControllers = @[navi1,navi2];
    
    window = [[UIWindow alloc] initWithFrame:CGRectMake(bounds.origin.x, bounds.origin.y-20, bounds.size.width, bounds.size.height+20)];

    window.hidden = NO;
    window.userInteractionEnabled=YES;
    //切割超出window的部分
    window.clipsToBounds = YES;
    
    window.rootViewController=tab;
    self.view.backgroundColor = [UIColor grayColor];
    
    [self.view addSubview:window];
    window = nil;
    
//    [self performSelector:@selector(back:) withObject:nil afterDelay:10];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
