//
//  Tab1ContentViewController.h
//  TestNestNavi
//
//  Created by juwenz on 13-11-19.
//  Copyright (c) 2013年 scics. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol backDelegate <NSObject>

-(void)back:(id)sender;

@end

@interface Tab1ContentViewController : UIViewController

-(IBAction)deep:(id)sender;

@property (nonatomic,copy) id<backDelegate> backDelegate;

@end
