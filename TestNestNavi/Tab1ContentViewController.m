//
//  Tab1ContentViewController.m
//  TestNestNavi
//
//  Created by juwenz on 13-11-19.
//  Copyright (c) 2013年 scics. All rights reserved.
//

#import "Tab1ContentViewController.h"
#import "Tab1SubViewController.h"

@interface Tab1ContentViewController ()

@end

@implementation Tab1ContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title = @"tab1";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initBackBtn];
}
-(void)initBackBtn{
    UIBarButtonItem *item1 =[[UIBarButtonItem alloc]initWithTitle:@"主页" style:UIBarButtonItemStylePlain target:self action:@selector(doBack:)];
    
    
    self.navigationItem.leftBarButtonItem=item1;
}
-(void)doBack:(id)sender{
    if ([_backDelegate respondsToSelector:@selector(back:)]) {
//        [self.view removeFromSuperview];
        [_backDelegate back:sender];
    }
}

-(IBAction)deep:(id)sender{
    Tab1SubViewController *subView = [[Tab1SubViewController alloc]init];
    [self.navigationController pushViewController:subView animated:YES];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touchbegan");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
