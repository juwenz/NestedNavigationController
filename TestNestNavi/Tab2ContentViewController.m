//
//  Tab2ContentViewController.m
//  TestNestNavi
//
//  Created by juwenz on 13-11-19.
//  Copyright (c) 2013年 scics. All rights reserved.
//

#import "Tab2ContentViewController.h"

@interface Tab2ContentViewController ()

@end

@implementation Tab2ContentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.title=@"tab2";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    // Do any additional setup after loading the view from its nib.
}

-(void)deep:(id)sender{
    NSLog(@"here");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
