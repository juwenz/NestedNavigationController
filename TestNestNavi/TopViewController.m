//
//  TopViewController.m
//  TestNestNavi
//
//  Created by juwenz on 13-11-19.
//  Copyright (c) 2013年 scics. All rights reserved.
//

#import "TopViewController.h"
#import "Tab1ContentViewController.h"
#import "Tab2ContentViewController.h"
#import "Module1ViewController.h"

@interface TopViewController (){
//    UINavigationController *navi1;
//    UINavigationController *navi2;
//    
//    UIWindow *win;
}

@end

@implementation TopViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}
-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"%@",self.view.subviews);
    NSLog(@"%f %f",self.view.frame.origin.y,self.view.frame.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoModule1:(id)sender {
    //方案1
    /*
     说明:
     主界面点击子模块时，新建一个UIWindow视图, 然后把该子模块的四个navigationcontroller初始化好以后，
     分别加入到UITabBarController中，并且作为UIWindow 的 rootViewController,最后把UIWindow的实例添加到当前页面中。
     结果是能够进去子模块，但是无法返回到主界面;
     */
    
    //新建UIWindow
    /*
    CGRect bounds = self.view.bounds;
//    UIWindow *win = [[UIWindow alloc]initWithFrame:CGRectMake(bounds.origin.x, bounds.origin.y-20, bounds.size.width, bounds.size.height)];
    UIWindow *win = [[UIWindow alloc]initWithFrame:bounds];
    win.backgroundColor = [UIColor redColor];
    Tab1ContentViewController *tab1 = [[Tab1ContentViewController alloc]init];
    Tab2ContentViewController *tab2 = [[Tab2ContentViewController alloc]init];
    
    //初始化两个tab的navigationcontroller
    UINavigationController *navi1 = [[UINavigationController alloc]initWithRootViewController:tab1];
    UINavigationController *navi2 = [[UINavigationController alloc]initWithRootViewController:tab2];
    
    //初始化tabbarController并加入两个navigationcontroller
    UITabBarController *tab = [[UITabBarController alloc]initWithNibName:@"UITabBarController" bundle:nil];
    tab.viewControllers = @[navi2,navi1];
    
    //设置window为可视，
    win.hidden = NO;
    win.userInteractionEnabled=YES;
    
    //设置window的rootviewController
    [self.navigationController setNavigationBarHidden:YES];
    win.rootViewController=tab;
    //添加到当前视图中
    [self.view addSubview:win];
     */
     
    
    
    //方案2
    /*
     说明：
     点击子模块后，不创建UIWindow，直接创建UITabBarController，
     然后直接添加到当前页面中。结果是切换tab时报错;
     */
    /*
    Tab1ContentViewController *tab1 = [[Tab1ContentViewController alloc]init];
    Tab2ContentViewController *tab2 = [[Tab2ContentViewController alloc]init];
    
    //初始化两个tab的navigationcontroller
    UINavigationController *navi1 = [[UINavigationController alloc]initWithRootViewController:tab1];
    UINavigationController *navi2 = [[UINavigationController alloc]initWithRootViewController:tab2];
    
    //初始化tabbarController并加入两个navigationcontroller
    UITabBarController *tab = [[UITabBarController alloc]initWithNibName:@"UITabBarController" bundle:nil];
    tab.viewControllers = @[navi1,navi2];
    
    //直接加入到当前界面
    [self.view addSubview:tab.view];
     */
    
    //方案3
    /*
     主界面点击子模块进入一个UIViewController，按照方案1新建一个UIWindow ， 最后 把window添加到当前页面中。结果也是无法返回主界面。
     */
    
    self.view.clipsToBounds = YES;
    Module1ViewController *module1VC = [[Module1ViewController alloc]init];
    [self.navigationController pushViewController:module1VC animated:YES];
     
     
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    NSLog(@"touch began");
}

@end
